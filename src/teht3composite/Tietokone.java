/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht3composite;

/**
 *
 * @author aleks
 */
import java.util.ArrayList;
import java.util.List;

public class Tietokone implements Laiteosa{

	//collection of laiteosa
	private List<Laiteosa> Laiteosa = new ArrayList<Laiteosa>();
	
	
	//adding shape to drawing
	public void add(Laiteosa s){
		this.Laiteosa.add(s);
	}
	
	//removing shape from drawing
	public void remove(Laiteosa s){
		Laiteosa.remove(s);
	}
	
	//removing all the shapes
	public void clear(){
		System.out.println("Clearing all the shapes from drawing");
		this.Laiteosa.clear();
	}


    @Override
    public int hinta() {
        int kokHinta =0;
        for(Laiteosa sh : Laiteosa){
            kokHinta = kokHinta+ sh.hinta();
	}
        return kokHinta;
    }

}
