/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht3composite;

/**
 *
 * @author aleks
 */
public class TestCompositePattern {

	public static void main(String[] args) {
		Laiteosa Emo = new Emolevy();
		Laiteosa Kotelo = new Kotelo();
		Laiteosa Muisti = new Muistipiiri();
                Laiteosa Näyttö = new Näyttö();
                Laiteosa Verkkokortti = new Verkkokortti();
                Laiteosa Virtalähde = new Virtalähde();
                Laiteosa Näytönohjain = new Näytönohjain();
                Laiteosa Prosessori = new Prosessori();
		
		Tietokone PC = new Tietokone();
                PC.add(Emo);
                PC.add(Kotelo);
                PC.add(Muisti);
                PC.add(Näyttö);
                PC.add(Verkkokortti);
                PC.add(Virtalähde);
                PC.add(Näytönohjain);
                PC.add(Prosessori);
                int kokhinta = PC.hinta();
                System.out.println("Tietokoneen kokonaishinta on:" + kokhinta );
                
                
                

	}

}
